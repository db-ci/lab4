import {promises} from 'fs';
import {Moeda, Cofrinho} from './entidades';

/*
export function salvarCofrinho(cofrinho: Cofrinho, arq: string){
    let json = JSON.stringify(cofrinho);
    fs.writeFile(arq, json, erro => {
        if(erro !== null)
            throw erro;
    }
});

export function lerCofrinho(nomeArq: string,
    callback: (erro: Error|null, dados?: Cofrinho) => void): void {
        fs.readFile(nomeArq,'utf-8', (erro, dados) => {
            if (erro !== null) {
                callback(erro);
            }
            try {
                const obj = JSON.parse(dados);
                const cofre = new Cofrinho();
                for(let i=0; i<obj._moedas.length; i++){
                    cofre.adicionar(new Moeda(obj._moedas[i]._valor, obj._moedas[i]._nome));
                }
                callback(null,cofre);
            } catch (e) {
                callback(e);
            }
        });
}
*/
export function lerCofrinhoAsync(nomeArq: string): Promise<Cofrinho> {
    return promises.readFile(nomeArq,'utf-8')
    .then(dados => JSON.parse(dados))
    .then(obj => {
        const cofre = new Cofrinho();
        for(let i=0; i<obj._moedas.length; i++){
            cofre.adicionar(new Moeda(obj._moedas[i]._valor, obj._moedas[i]._nome));
        }
        return cofre;
    });
}

export async function lerCofrinhoAsyncAwait(nomeArq: string): Promise<Cofrinho> {
    const dados = await promises.readFile(nomeArq,'utf-8');
    try {
        const obj = JSON.parse(dados);
        const cofre = new Cofrinho();
        for(let i=0; i<obj._moedas.length; i++){
            cofre.adicionar(new Moeda(obj._moedas[i]._valor, obj._moedas[i]._nome));
        }
        return cofre;
    } catch (err) {
        throw err;
    }
}


