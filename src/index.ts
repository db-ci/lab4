import { Cofrinho, Moeda } from "./entidades";
import { lerCofrinhoAsync, lerCofrinhoAsyncAwait } from "./persistencia";

async function main() {
    let cofre = new Cofrinho();
    console.log(cofre.adicionar(new Moeda(1, '1 real')));
    console.log(cofre.adicionar(new Moeda(10, '10 reais')));
    console.log(cofre.adicionar(new Moeda(0.5, '50 centavos')));
    console.log(cofre.adicionar(new Moeda(20, '20 reais')));

    /*
    try {
        salvarCofrinho(cofre,'meuCofrinho.json');
    } catch (erro) {
        console.log('Erro de escrita do arquivo:');
        console.log(erro);
    }
    */
    /*
    lerCofrinho('meuCofrinho.json', (erro,cofre) => {
        if (erro) {
        console.log('Erro de leitura do arquivo:');
        console.log(erro);
   }
   else {
       console.log(cofre!.calcularTotal());
    }
});
*/
lerCofrinhoAsync('meuCofrinho.json')
.then(cofre => console.log(cofre.calcularTotal()))
.catch(erro => {
    console.log('Erro na leitura do arquivo:');
    console.log(erro);
});

try {
    const cofre = await lerCofrinhoAsyncAwait('meuCofrinho.json');
    console.log(cofre.calcularTotal())
    console.log(cofre.getmenorMoeda())
    console.log(cofre.getMenorMoeda())
} catch (erro) {
    console.log('Erro na leitura do arquivo:');
    console.log(erro);
    }
}
main();