export class Moeda {
    constructor(
        public valor: number, 
        public nome: string
    ){}
       
    get getvalor(): number {
        return this.valor;
    }

    get getnome(): string {
        return this.nome;
    }
}

export class Cofrinho {
    public _moeda: Moeda[] = [];

    adicionar(moeda: Moeda): void {
        this._moeda.push(moeda);
    }

    calcularTotal(): number {
        return this._moeda.reduce((soma, moeda) => soma + moeda.valor, 0);
    }

    getmenorMoeda(): number{
        let moeda = this.getMenorMoeda();
        return moeda ? moeda.valor : 0;
    }

    getMenorMoeda(): Moeda | null {
        return this._moeda.length ? this._moeda.sort((a, b) => a.valor - b.valor)[0] : null;
    }
}
